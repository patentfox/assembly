#include <stdio.h>

void printfloat(char arr[]) {
   float *f = (float*) arr;
   printf("%f\n", *f);
}

int main() {
   char a1[] = {0x00, 0x00, 0x00, 0x00};
   char a2[] = {0x10, 0x00, 0x7f, 0x00};
   printfloat(a1);
   printfloat(a2);
   return 0;
}