segment .data
   a  dd    4
   b  dd    4.4
   c  times 10 dd 0
   d  dw    1, 2
   e  db    0xfb
   f  db    "Hello World", 0     ; array of bytes

segment .bss
   g  resd  1
   h  resd  10
   i  resb  100

segment .text
global main       ; let the linker know about main (gcc)

main:
   push  rbp         ; setup stack frame for main
   mov   rbp, rsp    ; set rbp to point to stack frame
   sub   rsp, 16     ; leave 16 byte room for local variable.
                     ; leave rsp on 16 byte boundary (don't yet know why)
                     ; remember that stack frows down
   xor   eax, eax    ; this is not clear
   leave 
   ret 