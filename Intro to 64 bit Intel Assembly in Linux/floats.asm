segment .data
   zero  dd    0.0
   neg1  dd    -1.0
   one   dd    1.0
   a     dd    0.75     ; should be 0000403F
   b     dd    3.0      ; should be 00004040