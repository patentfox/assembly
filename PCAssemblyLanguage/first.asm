; file: first.asm
; First assembly program. This program takes 2 integers as input
; and prints their sum.
;
; To create executable
; nasm -f elf first.asm
; gcc -o first first.o driver.c asm_io.o

%include "asm_io.inc"
;
; Initialized data is put in data segment
;
segment .data
;
; These labels refer to strings used for output
;
prompt1 db "Enter a number: ", 0    ; don't forget null terminator
prompt2 db "Enter another number: ", 0
outmsg1 db "You entered ", 0
outmsg2 db ", and ", 0
outmsg3 db ", their sum is ", 0

; uninitialized data is put in bss segment
;
segment .bss
;
; These labels refer to double words used to store the input
;
input1 resd 1
input2 resd 1

;
; code is put in the .text segment
;
segment .text
   global asm_main
asm_main:
   enter 0,0               ; setup routine
   pusha

   mov   eax, prompt1      ; print out prompt
   call  print_string

   call  read_int;         ; read integer
   mov   [input1], eax     ; store read integer to input1

   mov   eax, prompt2      ; print out prompt
   call  print_string

   call  read_int;
   mov   [input2], eax

   mov   eax, [input1]
   add   eax, [input2]
   mov   ebx, eax

   dump_regs   1                 ; print out register values
   dump_mem    2, outmsg1, 1     ; print out memory
;
; next print out message as a series of steps
;
   mov   eax, outmsg1
   call  print_string      ; print out first message
   mov   eax, [input1] 
   call  print_int         ; print first integer
   mov   eax, outmsg2
   call  print_string
   mov   eax, [input2]
   call  print_int
   mov   eax, outmsg3
   call  print_string
   mov   eax, ebx
   call  print_int
   call  print_nl          ; print new line

   popa
   mov   eax, 0            ; clear eax and return back to C
   leave
   ret